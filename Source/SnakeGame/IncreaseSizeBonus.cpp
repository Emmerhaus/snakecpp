// Fill out your copyright notice in the Description page of Project Settings.


#include "IncreaseSizeBonus.h"
#include "SnakeBase.h"

class ASnakeBase;

// Sets default values
AIncreaseSizeBonus::AIncreaseSizeBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AIncreaseSizeBonus::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AIncreaseSizeBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AIncreaseSizeBonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead) {
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (Snake) {
			Snake->BonusEvent(EEvent::INCREASE);
			this->Destroy();
		}
	}
}

