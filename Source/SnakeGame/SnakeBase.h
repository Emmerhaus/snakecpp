// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection {
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UENUM(BlueprintType)
enum class EEvent : uint8 {
	SPEED	UMETA(DisplayName="Speed"),
	INCREASE	UMETA(DisplayName="Size")
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(BlueprintReadWrite)
		float ElementSize;

	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY(BlueprintReadWrite)
		float MovementSpeed;

	UPROPERTY()
		EMovementDirection LastMoveDirection;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum = 1);
	UFUNCTION(BlueprintCallable)
	void Move();
	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
	UFUNCTION(BlueprintNativeEvent)
	void BonusEvent(EEvent event);
	void BonusEvent_Implementation(EEvent event);
};
