// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedBooster.h"
#include "SnakeBase.h"

class ASnakeBase;

// Sets default values
ASpeedBooster::ASpeedBooster()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpeedBooster::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpeedBooster::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeedBooster::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead) {
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (Snake) {
			Snake->BonusEvent(EEvent::SPEED);
			this->Destroy();
		}
	}
}

